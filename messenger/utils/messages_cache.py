from typing import Optional

MAX_SIZE = 100


class MessagesCache:
    def __init__(self):
        self.current_len = 0
        self.data = {}

    def add(self, chat_id: int, cursor: Optional[str], messages: list):
        if cursor is None or len(messages) > MAX_SIZE:
            return

        if self.current_len + len(messages) > MAX_SIZE:
            self.data = {}
            self.current_len = 0

        if chat_id in self.data:
            self.data[chat_id][cursor] = messages
        else:
            self.data[chat_id] = {cursor: messages}

        self.current_len += len(messages)

    def get(self, chat_id, cursor, limit) -> Optional[list]:
        if chat_id in self.data:
            if cursor in self.data[chat_id]:
                if len(self.data[chat_id][cursor]) <= limit:
                    return self.data[chat_id][cursor][:limit + 1]

    def clear(self, chat_id):
        if chat_id in self.data:
            del self.data[chat_id]
