from random import randrange


def get_random_id():
    # todo: delete after business-logic implemented
    return str(randrange(1, 100))


def get_random_messages(limit):
    # todo: delete after business-logic implemented
    return [{'text': f'message{i}'} for i in range(randrange(1, limit))]
