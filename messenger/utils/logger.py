import functools
import logging
import time


def log_func_time():
    def log(func):
        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            t1 = time.time()
            res = func(*args, **kwargs)
            t2 = time.time()
            timedelta = t2 - t1
            logging.debug(f' {func.__name__} resulted in {timedelta * 1000} milliseconds')

            return res

        return wrapper

    return log
