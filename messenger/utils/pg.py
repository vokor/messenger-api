import os
from asyncpgsa import PG
from types import SimpleNamespace
from typing import Union
from pathlib import Path
from configargparse import Namespace
from alembic import config, command
from aiohttp.web_app import Application
import logging

POSTGRES_DB = os.getenv("POSTGRES_DB", "messenger")
POSTGRES_USER = os.getenv("POSTGRES_USER", "user")
POSTGRES_PWD = os.getenv("POSTGRES_PWD", "hackme")
POSTGRES_HOST = os.getenv("POSTGRES_HOST", "localhost")

PROJECT_PATH = Path(__file__).parent.parent.resolve()
DB_URL = f"postgresql://{POSTGRES_USER}:{POSTGRES_PWD}@{POSTGRES_HOST}/{POSTGRES_DB}"
log = logging.getLogger(__name__)


async def setup_pg(app: Application):
    log.info('Connecting to database')

    app['pg'] = PG()
    await app['pg'].init(
        DB_URL,
        min_size=5,
        max_size=20
    )

    await app['pg'].fetchval('SELECT 1')
    log.info('Connected to database')

    await apply_migrations(app)

    try:
        yield
    finally:
        log.info('Disconnecting from database')
        await app['pg'].pool.close()
        log.info('Disconnected from database')


def make_alembic_config(cmd_opts: Union[Namespace, SimpleNamespace],
                        base_path: str = PROJECT_PATH):
    """
    Создает объект конфигурации alembic на основе аргументов командной строки,
    подменяет относительные пути на абсолютные.
    """
    # Подменяем путь до файла alembic.ini на абсолютный
    if not os.path.isabs(cmd_opts.config):
        cmd_opts.config = os.path.join(base_path, cmd_opts.config)

    alembic_config = config.Config(file_=cmd_opts.config, ini_section=cmd_opts.name,
                                   cmd_opts=cmd_opts)

    # Подменяем путь до папки с alembic на абсолютный
    alembic_location = alembic_config.get_main_option('script_location')
    if not os.path.isabs(alembic_location):
        alembic_config.set_main_option('script_location',
                                       os.path.join(base_path, alembic_location))
    if cmd_opts.pg_url:
        alembic_config.set_main_option('sqlalchemy.url', cmd_opts.pg_url)

    return alembic_config


async def apply_migrations(app: Application):
    cmd_options = SimpleNamespace(config=os.path.join('db', 'alembic.ini'), name='alembic', pg_url=DB_URL,
                                  raiseerr=False, x=None)
    alembic_config = make_alembic_config(cmd_options)
    command.upgrade(alembic_config, 'head')

    # todo: move to migrations:
    await app['pg'].fetchval('CREATE EXTENSION IF NOT EXISTS pg_trgm;')
    await app['pg'].fetchval('CREATE INDEX IF NOT EXISTS text_trgm_idx ON message USING gin (text gin_trgm_ops);')
