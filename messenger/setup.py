from setuptools import find_packages, setup

install_requires = [
    'SQLAlchemy~=1.4.25',
    'aiohttp~=3.7.4',
    'pydantic~=1.8.2',
    'ConfigArgParse~=1.5.3',
    'alembic~=1.7.4',
    'setuptools~=57.0.0',
    'psycopg2-binary~=2.9.1',
    'asyncpgsa~=0.27.1',
    'bcrypt~=3.2.0'
]

setup(
    name='messenger',
    platforms="all",
    packages=find_packages(exclude=['tests']),
    install_requires=install_requires,
    package_data={'': ['user_settings.csv', 'alembic.ini']},
    include_package_data=True,
    entry_points={
        'console_scripts': [
            'messenger-api = api.__main__:main',
            'messenger-db = db.__main__:main'
        ]
    }
)
