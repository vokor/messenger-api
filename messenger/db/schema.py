from sqlalchemy import (MetaData, Column, DateTime, ForeignKey,
                        Integer, BigInteger, String, Table, func, text)

convention = {
    'all_column_names': lambda constraint, table: '_'.join([
        column.name for column in constraint.columns.values()
    ]),
    'ix': 'ix__%(table_name)s__%(all_column_names)s',
    'uq': 'uq__%(table_name)s__%(all_column_names)s',
    'ck': 'ck__%(table_name)s__%(constraint_name)s',
    'fk': 'fk__%(table_name)s__%(all_column_names)s__%(referred_table_name)s',
    'pk': 'pk__%(table_name)s'
}

metadata = MetaData(naming_convention=convention)

user_table = Table(
    'user',
    metadata,
    Column('id', Integer, primary_key=True),
    Column('login', String, nullable=False, unique=True),
    Column('password', String, nullable=False),
    Column('created_at', DateTime, default=func.now())
)

chat_table = Table(
    'chat',
    metadata,
    Column('id', Integer, primary_key=True),
    Column('name', String, nullable=False),
    Column('created_at', DateTime, default=func.now())
)

chat_user_table = Table(
    'chat_user',
    metadata,
    Column('user_id', Integer, ForeignKey('user.id'), primary_key=True),
    Column('chat_id', Integer, ForeignKey('chat.id'), primary_key=True),
    Column('user_name', String, nullable=False),
    Column('created_at', DateTime, default=func.now())
)

message_table = Table(
    'message',
    metadata,
    Column('id', BigInteger, primary_key=True),
    Column('text', String, nullable=False),
    Column('chat_id', Integer, ForeignKey('chat.id'), index=True),
    Column('user_from', Integer, ForeignKey('user.id'), index=True),
    Column('created_at', DateTime, server_default=text('now()'))
)

user_session_table = Table(
    'user_session',
    metadata,
    Column('id', BigInteger, primary_key=True),
    Column('user_id', Integer, ForeignKey('user.id')),
    Column('created_at', DateTime, default=func.now())
)
