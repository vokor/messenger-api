from aiohttp import web

routes = web.RouteTableDef()


@routes.get('/ping_db')
async def ping_db(request: web.Request):
    await request.app['pg'].fetchval('SELECT 1')
    return web.Response(text='pong')


@routes.get('/ping')
async def ping(request: web.Request):
    return web.Response(text='pong')
