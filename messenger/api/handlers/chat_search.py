import asyncio
from aiohttp import web
from http import HTTPStatus
from api.models.input import MessageGetQueryParams
from api.handlers.query import search_messages_query
from api.models.output import ChatGetMessageResponse

routes = web.RouteTableDef()  # Routes to be added to aiohttp.web.Application
tasks_result = {}
tasks = {}


@routes.post('/v1/chats/search')
async def search_messages(request: web.Request):
    req_data = await request.json()

    if 'message' not in req_data or len(req_data['message']) <= 3:
        return web.json_response({'message': 'Bad parameters'}, status=HTTPStatus.BAD_REQUEST)

    params = MessageGetQueryParams(**request.rel_url.query)

    task_id = len(tasks)  # todo get from db
    tasks_result[task_id] = {}
    task = asyncio.create_task(find_messages(request, params, req_data, task_id))
    tasks[task_id] = task

    return web.json_response({'task_id': task_id}, status=HTTPStatus.CREATED)


@routes.get('/v1/chats/search/status/{task_id}')
async def get_search_task_status(request: web.Request):
    task_id = int(request.match_info['task_id'])

    if task_id not in tasks:
        return web.json_response({'message': 'task not found'}, status=HTTPStatus.NOT_FOUND)
    try:
        tasks[task_id].result()
        return web.json_response({'status': 'SUCCESS'})
    except asyncio.InvalidStateError:
        return web.json_response({'status': 'IN_PROCESS'})
    except:
        return web.json_response({'status': 'FAILED'})


@routes.get('/v1/chats/search/{task_id}/messages')
async def get_search_task_result(request: web.Request):
    task_id = int(request.match_info['task_id'])

    if task_id not in tasks_result:
        return web.json_response({'message': 'task not found'}, status=HTTPStatus.NOT_FOUND)

    messages = tasks_result[task_id]['messages']
    cursor = ChatGetMessageResponse.Cursor(iterator=tasks_result[task_id]['last'])

    return web.json_response({'messages': messages, 'next': cursor.dict()})


async def find_messages(request, params, req_data, task_id):
    messages = []

    session_id = int(request.headers['session_id'])
    user_id = request.app['sessions_cache'][session_id]['user_id']

    await asyncio.sleep(3)  # todo: delete after been tested

    query = search_messages_query(req_data['message'], user_id, params.limit, params.from_)
    con = request.app['pg']

    tasks_result[task_id]['messages'] = []
    last = 0

    for row in await con.fetch(query):
        await asyncio.sleep(1)  # todo: delete after been tested
        item = {'text': row['text'], 'chat_id': row['chat_id']}
        last = row['id']
        tasks_result[task_id]['messages'].append(item)

    tasks_result[task_id]['last'] = str(int(last) + 1)
