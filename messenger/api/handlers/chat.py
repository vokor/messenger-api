from aiohttp import web
from http import HTTPStatus
from zoneinfo import ZoneInfo
from datetime import datetime

from api.models.input import (ChatCreateModel, UserCreateModel, MessageGetQueryParams,
                              MessagePostQueryParams, MessageCreateModel)
from api.models.output import (ChatCreateResponse, ChatJoinResponse,
                               ChatSendMessageResponse, ChatGetMessageResponse)
from utils.messages_cache import MessagesCache
from utils.logger import log_func_time
from api.services.user_settings.user_settings_service import UserSettingsService
from api.handlers.query import (create_chat_query, add_user_to_chat_query, get_messages_query, send_message_query,
                                check_if_chat_exists_query)
from asyncpg.exceptions import InvalidCatalogNameError

routes = web.RouteTableDef()  # Routes to be added to aiohttp.web.Application
cache = MessagesCache()


@routes.post('/v1/chats')
@log_func_time()
async def create_chat(request: web.Request):
    req_data = await request.json()

    chat = ChatCreateModel(**req_data)

    con = request.app['pg']
    query = create_chat_query(chat.chat_name)
    chat_id = await con.fetchval(query)

    data = ChatCreateResponse(chat_id=chat_id).dict()

    return web.json_response(data, status=HTTPStatus.CREATED)


@routes.post('/v1/chats/{chat_id}/users')
@log_func_time()
async def add_user_to_chat(request: web.Request):
    req_data = {}
    if request.body_exists:
        req_data = await request.json()
    chat_id = request.match_info['chat_id']
    user = UserCreateModel(**req_data)

    if not chat_id.isnumeric():
        raise web.HTTPNotFound

    con = request.app['pg']

    session_id = int(request.headers['session_id'])
    user_id = request.app['sessions_cache'][session_id]['user_id']

    query = check_if_chat_exists_query(chat_id)
    chat = await con.fetchval(query)

    if chat is None:
        return web.json_response({'message': 'chat not found'}, status=HTTPStatus.NOT_FOUND)

    query = add_user_to_chat_query(user.user_name, chat_id, user_id)
    await con.fetchval(query)

    data = ChatJoinResponse(user_id=user_id).dict()

    return web.json_response(data, status=HTTPStatus.CREATED)


@routes.get('/v1/chats/{chat_id}/messages')
@log_func_time()
async def get_messages(request: web.Request) -> web.Response:
    chat_id = request.match_info['chat_id']
    # user_id = request.headers.get('user_id')

    params = MessageGetQueryParams(**request.rel_url.query)

    try:
        con = request.app['pg']
    except (InvalidCatalogNameError, OSError):
        return get_bot_answer(1)  # todo: get from session

    if not chat_id.isnumeric():
        raise web.HTTPNotFound

    chat_id = int(chat_id)

    query = check_if_chat_exists_query(chat_id)
    chat = await con.fetchval(query)

    if chat is None:
        return web.json_response({'message': 'chat not found'}, status=HTTPStatus.NOT_FOUND)

    cached_messages = cache.get(chat_id, params.from_, params.limit)

    if cached_messages is not None:
        messages = cached_messages
    else:
        query = get_messages_query(chat_id, params.limit, params.from_)
        data = await con.fetch(query)
        messages = []
        if data is not None:
            for row in data:
                messages.append(ChatGetMessageResponse.Message(text=row[0]))

        cache.add(chat_id, params.from_, messages)

    iterator = len(messages) + int(params.from_) if params.from_ is not None else len(messages) + 1

    cursor = ChatGetMessageResponse.Cursor(iterator=iterator)
    response = ChatGetMessageResponse(messages=messages, next=cursor).dict(exclude_none=True)

    return web.json_response(response)


@routes.post('/v1/chats/{chat_id}/messages')
@log_func_time()
async def send_message(request: web.Request):
    chat_id = request.match_info['chat_id']
    req_data = {}
    if request.body_exists:
        req_data = await request.json()

    params = MessagePostQueryParams(**request.rel_url.query)
    message = MessageCreateModel(**req_data)

    con = request.app['pg']

    if not chat_id.isnumeric() or not params.user_id.isnumeric():
        raise web.HTTPNotFound

    chat_id = int(chat_id)
    query = check_if_chat_exists_query(chat_id)
    chat = await con.fetchval(query)

    if chat is None:
        return web.json_response({'message': 'chat not found'}, status=HTTPStatus.NOT_FOUND)

    cache.clear(chat_id)

    user_id = int(params.user_id)

    query = send_message_query()
    message_id = await con.fetchval(query, message.message, chat_id, user_id)

    data = ChatSendMessageResponse(message_id=message_id).dict()

    return web.json_response(data, status=HTTPStatus.CREATED)


@log_func_time()
def get_bot_answer(user_id: str, db) -> web.Response:
    def get_time_of_day_message(timezone):
        dt = datetime.now(tz=ZoneInfo(timezone))
        if 4 < dt.hour < 12:
            return 'Доброе утро'
        elif 12 < dt.hour < 17:
            return 'Добрый день'
        elif 17 < dt.hour < 22:
            return 'Добрый вечер'
        else:
            return 'Доброй ночи'

    user_settings_service = UserSettingsService()
    settings = user_settings_service.get_user_settings(user_id, db)

    if settings is None:
        greetings_message = 'Здравствуйте!'
    else:
        greetings_message = f'{get_time_of_day_message(settings.timezone)}, {settings.name}!'

    messages = [
        {'text': f"""{greetings_message}
        С вами говорит чат-бот.
        Кажется, что-то сломалось.
        Попробуйте еще раз позднее."""}]

    data = ChatGetMessageResponse(messages=messages).dict(exclude_none=True)

    return web.json_response(data)
