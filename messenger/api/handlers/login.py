import time
from http import HTTPStatus
from aiohttp import web
from api.models.input import UserRegisterModel, UserLoginModel
from api.handlers.query import (create_user_query, check_if_login_exists_query,
                                get_user_query, create_user_session_query)
from bcrypt import hashpw, gensalt, checkpw

routes = web.RouteTableDef()


@routes.post('/register')
async def register(request: web.Request):
    req_data = await request.json()
    user = UserRegisterModel(**req_data)

    con = request.app['pg']

    query = check_if_login_exists_query(user.login)
    user_exists = await con.fetchval(query)

    if user_exists is not None:
        return web.json_response({'message': 'login already exists'},
                                 status=HTTPStatus.BAD_REQUEST)

    hashed_password = hashpw(user.password.encode('UTF-8'), gensalt())

    query = create_user_query()
    user_id = await con.fetchval(query, user.login,
                                 hashed_password.decode('UTF-8'))

    query = create_user_session_query(user_id)
    session_id = await con.fetchval(query)

    request.app['sessions_cache'][session_id] = \
        {'user_id': user_id, 'last_action': time.time()}  # todo: move to db

    return web.json_response({'session_id': session_id},
                             status=HTTPStatus.CREATED)


@routes.post('/login')
async def login(request: web.Request):
    req_data = await request.json()
    user = UserLoginModel(**req_data)

    con = request.app['pg']

    query = get_user_query(user.login)
    bd_user = await con.fetchrow(query)

    if bd_user is None:
        return web.json_response({'message': 'user does not exist'},
                                 status=HTTPStatus.BAD_REQUEST)

    pw1 = user.password.encode('UTF-8')
    pw2 = bd_user['password'].encode('UTF-8')
    if checkpw(pw1, pw2) is False:
        return web.json_response({'message': 'wrong password'},
                                 status=HTTPStatus.BAD_REQUEST)

    query = create_user_session_query(bd_user['id'])
    session_id = await con.fetchval(query)

    # todo: move to db
    request.app['sessions_cache'][session_id] = {'user_id': bd_user['id'],
                                                 'last_action': time.time()}

    return web.json_response({'session_id': session_id})


@routes.post('/logout')
async def logout(request: web.Request):
    if 'session_id' not in request.headers:
        return web.json_response({'message': 'Session header not specified'}, status=HTTPStatus.BAD_REQUEST)

    session_id = int(request.headers['session_id'])
    if session_id not in request.app['sessions_cache']:
        return web.json_response({'message': 'Session not found'}, status=HTTPStatus.NOT_FOUND)

    del request.app['sessions_cache'][session_id]

    return web.json_response({'message': 'logged out'})
