from typing import Optional


def check_if_login_exists_query(login: str):
    return f'''
                select 1
                from "user"
                where (login = '{login}');
            '''


def get_user_query(login: str) -> str:
    return f'''
                select id, password from "user"
                where login = '{login}';
            '''


def create_user_query() -> str:
    return f'''
                insert into "user" (login, password)
                values ($1, $2)
                returning id;
            '''


def create_user_session_query(user_id: int) -> str:
    return f'''
                insert into "user_session" (user_id)
                values ('{user_id}')
                returning id;
            '''


def create_chat_query(chat_name: str) -> str:
    return f'''
                insert into "chat" (name)
                values ('{chat_name}')
                returning id;
            '''


def add_user_to_chat_query(user_name: str, chat_id: int, user_id: int) -> str:
    return f'''
                insert into "chat_user" (user_id, chat_id, user_name)
                values ('{user_id}', '{chat_id}', '{user_name}')
                returning user_id;
            '''


def get_messages_query(chat_id: int, limit: int, from_: Optional[int]) -> str:
    if from_ is None:
        from_ = 0
    return f'''
                select text from message 
                where (id >= {from_} and chat_id = '{chat_id}')
                limit {limit}
            '''


def send_message_query():
    return f'''
                insert into message (text, chat_id, user_from)
                values ($1,$2,$3)
                returning id;
            '''


def check_if_chat_exists_query(chat_id: int):
    return f'''
                select 1
                from chat
                where id = {chat_id};
            '''


def check_user_in_chat_exists_query(chat_id: int, user_id: int):
    return f'''
                select 1
                from chat_user
                where (user_id = {user_id} and chat_id = {chat_id});
            '''


def search_messages_query(text: str, user_id: int, limit: int, from_: Optional[int]):
    if from_ is None:
        from_ = 0
    return f'''
                select m.text, m.chat_id, m.id
                from message m
                inner join chat_user cu
                    on m.chat_id = cu.chat_id
                where cu.user_id = {user_id}
                    and m.text % '{text}'
                    and m.id >= {from_}
                order by m.created_at DESC
                limit {limit};
            '''
