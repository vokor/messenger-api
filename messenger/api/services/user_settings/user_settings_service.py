import csv
from typing import Optional
import os
from asyncpg.exceptions import InvalidCatalogNameError

# todo: move to config
USER_SETTINGS_FILE_NAME = 'user_settings.csv'


class UserSettings:
    def __init__(self, name: str, timezone: str):
        self.name = name
        self.timezone = timezone

    def get_user_greetings_string(self):
        pass


class UserSettingsService:
    def get_user_settings(self, user_id, db) -> Optional[UserSettings]:
        try:
            return self.get_from_db(user_id, db)
        except (InvalidCatalogNameError, OSError):
            return self.get_from_file(user_id)

    @staticmethod
    def get_from_db(user_id, db) -> UserSettings:
        data = db.get(user_id)
        return UserSettings(name=data['name'], timezone=data['timezone'])

    @staticmethod
    def get_from_file(user_id) -> Optional[UserSettings]:
        full_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), USER_SETTINGS_FILE_NAME)
        with open(full_path, newline='') as csv_file:
            reader = csv.DictReader(csv_file)
            for row in reader:
                if row['user_id'] == user_id:
                    return UserSettings(name=row['name'], timezone=row['timezone'])

            return None
