import os

from aiohttp import web
from api.middleware.middleware import (exception_middleware, auth_middleware)
from api.handlers.chat import routes as chat_routes
from api.handlers.ping import routes as db_routes
from api.handlers.login import routes as login_routes
from api.handlers.chat_search import routes as chat_search_routes
import logging
from functools import partial
from utils.pg import setup_pg
from asyncio import get_event_loop

AUTH_DISABLED = os.getenv("AUTH_DISABLED", "false")


def init_app():
    middlewares = [
        exception_middleware,
        auth_middleware
    ]

    routes = [
        chat_routes,
        db_routes,
        login_routes,
        chat_search_routes
    ]

    app = web.Application(middlewares=middlewares)

    app.cleanup_ctx.append(partial(setup_pg))

    for route in routes:
        app.add_routes(route)

    app['sessions_cache'] = {}
    app['auth_disabled'] = AUTH_DISABLED == 'true'
    app['event_loop'] = get_event_loop()
    return app


def main():
    logging.basicConfig(level=logging.INFO)
    app = init_app()
    web.run_app(app, access_log_format=" :: %r %s %T %t")


if __name__ == "__main__":
    main()
