import datetime
import time
from http import HTTPStatus
from aiohttp import web, web_exceptions
from pydantic import ValidationError
import logging
from asyncpg.exceptions import InvalidCatalogNameError

AUTH_IGNORED_PATHS = [
    '/register',
    '/login',
    '/ping',
    '/ping_db'
]


@web.middleware
async def exception_middleware(request: web.Request, handler):
    def get_error_response(message: str, status: int) -> web.Response:
        return web.json_response({'message': message}, status=status)

    logger = logging.getLogger('log')
    logger.disabled = False
    logger.setLevel(logging.DEBUG)

    try:
        logger.debug(datetime.datetime.now())

        req_body = None
        if request.body_exists:
            req_body = await request.json()
        logger.debug(f'request: {request}; body={req_body}; query={request.query_string}; headers={request.headers};')

        resp = await handler(request)

        logger.debug(f'response: {resp.status} {resp.body}')
        return resp
    except ValidationError as ex:
        logging.exception(ex)
        return get_error_response('Bad parameters', HTTPStatus.BAD_REQUEST)
    except web_exceptions.HTTPForbidden as ex:
        logging.exception(ex)
        return get_error_response('Not authenticated', ex.status_code)
    except web_exceptions.HTTPException as ex:
        logging.exception(ex)
        return get_error_response(ex.text, ex.status_code)
    except (InvalidCatalogNameError, OSError) as ex:
        logging.exception(ex)
        return get_error_response('Service is unavailable. Try again later', HTTPStatus.SERVICE_UNAVAILABLE)
    except Exception as ex:
        logging.exception(ex)
        return get_error_response('Unexpected error', HTTPStatus.INTERNAL_SERVER_ERROR)


@web.middleware
async def auth_middleware(request: web.Request, handler):
    if request.app['auth_disabled'] is True or request.path in AUTH_IGNORED_PATHS:
        return await handler(request)

    if 'session_id' not in request.headers:
        raise web_exceptions.HTTPForbidden()

    sessions_cache = request.app['sessions_cache']
    session_id = int(request.headers['session_id'])

    if session_id not in sessions_cache:
        return web.json_response({'message': 'session not found'}, status=HTTPStatus.NOT_FOUND)

    diff = time.time() - sessions_cache[session_id]['last_action']
    if diff < 0.5:
        raise web.HTTPTooManyRequests
    sessions_cache[session_id]['last_action'] = time.time()

    return await handler(request)
