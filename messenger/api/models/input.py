from aiohttp import web_exceptions

from typing import Optional
from pydantic import BaseModel, constr, Field, validator, ValidationError


class UserRegisterModel(BaseModel):
    login: constr(min_length=1, max_length=255)
    password: constr(min_length=6)
    check_password: constr(min_length=6)

    @validator('check_password')
    def check_passwords_are_same(cls, v, values):
        if v != values['password']:
            raise ValueError('passwords are not the same')

        return v


class UserLoginModel(BaseModel):
    login: str
    password: str


class ChatCreateModel(BaseModel):
    chat_name: constr(min_length=1, max_length=255)


class UserCreateModel(BaseModel):
    user_name: constr(min_length=1, max_length=255)


class MessageGetQueryParams(BaseModel):
    limit: int
    from_: Optional[str] = Field(alias='from')

    @validator('limit')
    def validate_limit_value(cls, v):
        if v <= 0 or v > 1000:
            raise ValueError('limit should from 1 to 1000')
        return v


class MessagePostQueryParams(BaseModel):
    user_id: constr(min_length=1)


class MessageCreateModel(BaseModel):
    message: str
