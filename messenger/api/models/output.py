from pydantic import BaseModel
from typing import Optional


class ChatCreateResponse(BaseModel):
    chat_id: str


class ChatJoinResponse(BaseModel):
    user_id: str


class ChatSendMessageResponse(BaseModel):
    message_id: str


class ChatGetMessageResponse(BaseModel):
    class Message(BaseModel):
        text: str

    class Cursor(BaseModel):
        iterator: str

    messages: list[Message]
    next: Optional[Cursor]
