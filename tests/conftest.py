import pytest
import sqlalchemy_utils
from messenger.api.__main__ import init_app
from unittest.mock import patch

PG_URL = 'postgresql://user:hackme@localhost:5432/messenger_test'


@pytest.fixture
async def api_client(aiohttp_client):
    with patch('messenger.utils.pg.DB_URL', PG_URL):
        from messenger.utils.pg import setup_pg as patched_setup
        with patch('messenger.api.__main__.setup_pg', patched_setup):
            sqlalchemy_utils.create_database(PG_URL)
            app = init_app()
            client = await aiohttp_client(app)
            yield client
            if sqlalchemy_utils.database_exists(PG_URL):
                sqlalchemy_utils.drop_database(PG_URL)
