import asyncio
import http
from .test_login import get_register_user_json


async def register_user(api_client, user_name):
    response = await api_client.post('/register',
                                     json=get_register_user_json(user_name, 'password'))

    json = await response.json()
    return json['session_id']


async def get_create_default_chat(session_id, api_client):
    await asyncio.sleep(0.5)
    headers = {'session_id': str(session_id)}
    response = await api_client.post('/v1/chats', json={'chat_name': 'chat_name'}, headers=headers)
    return response


async def test_create_chat_returns_201(api_client):
    session_id = await register_user(api_client, 'user')
    response = await get_create_default_chat(session_id, api_client)

    assert response.status == http.HTTPStatus.CREATED


async def add_user_to_chat(api_client, user_name, chat_id, session_id):
    headers = {'session_id': str(session_id)}

    await asyncio.sleep(0.5)
    response = await api_client.post(f'/v1/chats/{chat_id}/users', json={'user_name': user_name}, headers=headers)

    return response


async def test_add_user_to_chat_returns_201(api_client):
    session_id = await register_user(api_client, 'user')
    response = await get_create_default_chat(session_id, api_client)

    json = await response.json()
    chat_id = json['chat_id']

    response = await add_user_to_chat(api_client, 'user', chat_id, session_id)
    assert response.status == http.HTTPStatus.CREATED


async def test_add_two_users_to_chat(api_client):
    session_id1 = await register_user(api_client, 'user1')
    response = await get_create_default_chat(session_id1, api_client)

    json = await response.json()
    chat_id = json['chat_id']

    session_id2 = await register_user(api_client, 'user2')
    await add_user_to_chat(api_client, 'user1', chat_id, session_id1)
    await add_user_to_chat(api_client, 'user2', chat_id, session_id2)


async def send_message(api_client, chat_id, user_id, session_id):
    await asyncio.sleep(1)
    headers = {'session_id': str(session_id)}
    response = await api_client.post(f'/v1/chats/{chat_id}/messages?user_id={user_id}',
                                     json={'message': 'test_message'}, headers=headers)

    return response


async def get_messages(api_client, chat_id, session_id):
    await asyncio.sleep(1)
    headers = {'session_id': str(session_id)}
    response = await api_client.get(f'/v1/chats/{chat_id}/messages?limit=100', headers=headers)

    return response


async def test_add_two_user_to_chat_and_send_messages(api_client):
    session_id1 = await register_user(api_client, 'user1')
    response = await get_create_default_chat(session_id1, api_client)

    json = await response.json()
    chat_id = json['chat_id']

    session_id2 = await register_user(api_client, 'user2')
    resp = await add_user_to_chat(api_client, 'user1', chat_id, session_id1)
    json = await resp.json()
    user_id1 = json['user_id']

    resp = await add_user_to_chat(api_client, 'user2', chat_id, session_id2)
    json = await resp.json()
    user_id2 = json['user_id']

    await send_message(api_client, chat_id, user_id1, session_id1)
    await send_message(api_client, chat_id, user_id2, session_id2)

    resp1 = await get_messages(api_client, chat_id, session_id1)
    json = await resp1.json()
    assert len(json['messages']) == 2

