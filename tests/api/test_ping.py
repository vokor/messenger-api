import http
import sqlalchemy_utils
from tests.conftest import PG_URL


async def test_ping_returns_200(api_client):
    response = await api_client.get('/ping')
    assert response.status == http.HTTPStatus.OK


async def test_ping_db_returns_503(api_client):
    sqlalchemy_utils.drop_database(PG_URL)
    response = await api_client.get('/ping_db')
    assert response.status == http.HTTPStatus.SERVICE_UNAVAILABLE


async def test_ping_db_returns_200(api_client):
    response = await api_client.get('/ping_db')
    assert response.status == http.HTTPStatus.OK
