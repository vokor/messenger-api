import asyncio
import http


def get_register_user_json(login: str, password: str):
    return {'login': login,
            'password': password,
            'check_password': password}


def get_login_user_json(login: str, password: str):
    return {'login': login, 'password': password}


async def test_register_returns_201(api_client):
    response = await api_client.post('/register',
                                     json=get_register_user_json('user', 'password'))

    assert response.status == http.HTTPStatus.CREATED


async def test_login_registered_user_returns_200(api_client):
    login, password = 'user', 'password'
    await api_client.post('/register', json=get_register_user_json(login, password))
    response = await api_client.post('/login', json=get_login_user_json(login, password))

    assert response.status == http.HTTPStatus.OK


async def test_register_duplicate_user_returns_400(api_client):
    login, password = 'user', 'password'
    await api_client.post('/register', json=get_register_user_json(login, password))
    response = await api_client.post('/register', json=get_register_user_json(login, password))

    assert response.status == http.HTTPStatus.BAD_REQUEST


async def test_login_with_wrong_password_returns_400(api_client):
    login, password = 'user', 'password'
    await api_client.post('/register', json=get_register_user_json(login, password))
    response = await api_client.post('/login', json=get_login_user_json(login, 'wrong_password'))

    assert response.status == http.HTTPStatus.BAD_REQUEST


async def test_login_user_does_not_exist_returns_400(api_client):
    login, password = 'user', 'password'
    response = await api_client.post('/login', json=get_login_user_json(login, password))

    assert response.status == http.HTTPStatus.BAD_REQUEST


async def test_logout_registered_user_returns_200(api_client):
    login, password = 'user', 'password'
    response = await api_client.post('/register', json=get_register_user_json(login, password))
    json = await response.json()
    session_id = json['session_id']
    await asyncio.sleep(1)
    response = await api_client.post('/logout', headers={'session_id': str(session_id)})
    assert response.status == http.HTTPStatus.OK
